import collections
from lib.enum import Enum
from price.point import Point
from price.pivot import Pivot, BarSide
import psycopg2
from psycopg2.extras import NamedTupleConnection
from price.db import Db

PitchforkType = Enum('PitchforkType', ANDREWS=1, SCHIFF=2, MOD_SCHIFF=3)
PitchforkPattern = Enum('PitchforkPattern', LOW_HIGH_LOW=1, HIGH_LOW_HIGH=2)

Bar = collections.namedtuple('Bar', 'bar_seq open high low close ' +
                             'high_seq low_seq bar_time')


def mark_pivots(lows, highs, p, color):
    for bar in lows:
        x = bar.low_seq
        y1 = str(bar.low - .25)
        y2 = str(bar.low)
        p.write('set arrow from first {},{} to first {},{} '.
                format(x, y1, x, y2) +
                'filled linecolor rgb "{}"\n'.format(color))
    for bar in highs:
        x = bar.high_seq
        y1 = str(bar.high + .25)
        y2 = str(bar.high)
        p.write('set arrow from first {},{} to first {},{} '.
                format(x, y1, x, y2) +
                'filled linecolor rgb "{}"\n'.format(color))


class Window:
    def __init__(self, bars=[]):
        self.bars = bars
        if len(self.bars) >= 2:
            self.bar_duration = self.bars[1].bar_seq - self.bars[0].bar_seq

    @staticmethod
    def load(vehicle_id, duration=5, bars=1200):
        cur = Db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        try:
            cur.execute("select bar_seq, open, high, low, close, " +
                        "high_seq, low_seq, bar_time from bar " +
                        "where vehicle_id = %s and duration = %s " +
                        "order by bar_seq desc " +
                        "limit %s",
                        (vehicle_id, duration, bars))
            bars = cur.fetchall()
            bars.reverse()
            self = Window(bars)
            self.calc_major_pivots()
            return self
        finally:
            cur.connection.commit()
            cur.close()

    def seq_to_index(self, seq):
        return int((seq - self.bars[0].bar_seq) / self.bar_duration)

    def len(self):
        return len(self.bars)

    @property
    def last_bar(self):
        return self.bars[-1].bar_seq + self.bar_duration - 1

    def find_all_pivots(self):
        if len(self.bars) < 3:
            return None
        got = []
        for bx in range(1, len(self.bars) - 1):
            prev = self.bars[bx - 1]
            cur = self.bars[bx]
            next = self.bars[bx + 1]
            if ((prev.low < cur.low < next.low or
                 prev.low > cur.low > next.low or
                 (cur.low > prev.low and cur.low > next.low)) and
                (prev.high < cur.high < next.high or
                 prev.high > cur.high > next.high or
                 (cur.high < prev.high and cur.high < next.high))):
                continue   # these are not pivots
            got.append(cur)
        return Window(got)

    def is_local_low(self, bar_seq):
        bx = self.seq_to_index(bar_seq)
        prev = self.bars[bx - 1]
        bar = self.bars[bx]
        next = self.bars[bx + 1]
        return bar.low < prev.low and bar.low < next.low

    def is_local_high(self, bar_seq):
        bx = self.seq_to_index(bar_seq)
        prev = self.bars[bx - 1]
        bar = self.bars[bx]
        next = self.bars[bx + 1]
        return bar.high > prev.high and bar.high > next.high

    def estimate_wander1(self):
        lowest = highest = None
        for bar in self.bars[:50]:
            if lowest is None or lowest > bar.low:
                lowest = bar.low
            if highest is None or highest < bar.high:
                highest = bar.high
        return highest - lowest

    # like a keltner channel band
    def estimate_wander2(self):
        range = 0
        for bar in self.bars[:20]:
            range += bar.high - bar.low
        return range / 5

    # TODO: tune retracement threshold parameter
    # Might want to decay threshold as time accrues?
    def calc_major_pivots(self, start_index=0, wander=None,
                          out=None):
        lows, highs = [], []
        trailing_low = self.bars[start_index]
        trailing_high = self.bars[start_index]

        # TODO: replace training phase with parameters
        # need historical data for what counts as a swing

        if wander is None:
            wander = self.estimate_wander2()
        assert wander > 0
        prev_low = self.bars[start_index].low - wander
        prev_high = self.bars[start_index].high + wander

        for bar in self.bars:
            if out is not None:
                out.write('set arrow from first {},{} to first {},{} nohead '.
                          format(bar.bar_seq - 1, prev_low,
                                 bar.bar_seq, prev_low) +
                          'linecolor rgb "{}"\n'.format("yellow"))
                out.write('set arrow from first {},{} to first {},{} nohead '.
                          format(bar.bar_seq - 1, prev_high,
                                 bar.bar_seq, prev_high) +
                          'linecolor rgb "{}"\n'.format("yellow"))

            if bar.low < prev_low:
                prev_low = bar.low
                highest = None
                if bar.bar_seq < trailing_high.bar_seq:
                    highest = trailing_high
                else:
                    for bx in range(self.seq_to_index(trailing_low.bar_seq),
                                    self.seq_to_index(bar.bar_seq)):
                        if (highest is None or
                            highest.high < self.bars[bx].high):
                            highest = self.bars[bx]
                if highest is not None and highest.high > trailing_low.high:
                    if trailing_high != highest:
                        highs.append(highest)
                        trailing_high = highest
                if prev_high > bar.high + wander:
                    prev_high = bar.high + wander
            elif bar.high > prev_high:
                prev_high = bar.high
                lowest = None
                if bar.bar_seq < trailing_low.bar_seq:
                    lowest = trailing_low
                else:
                    for bx in range(self.seq_to_index(trailing_high.bar_seq),
                                    self.seq_to_index(bar.bar_seq)):
                        if lowest is None or lowest.low > self.bars[bx].low:
                            lowest = self.bars[bx]
                if lowest is not None and lowest.low < trailing_high.low:
                    if trailing_low != lowest:
                        lows.append(lowest)
                        trailing_low = lowest
                if prev_low < bar.low - wander:
                    prev_low = bar.low - wander
        # Could try adaptive wander adjustment like such as
        # if len(lows) < 3 or len(highs) < 3:
        #     self.calc_major_pivots(start_index=start_index,
        #                            wander=wander/2,
        #                            out=out, skip_first=skip_first)
        # else:
        #     self.lows, self.highs = lows, highs
        self.lows, self.highs = lows, highs

    # TODO: The height here should be Pitchfork.height instead?
    def pivot_lhl(self):
        abc = []
        low, high = self.lows, self.highs
        bc_tallest = None
        for ax in low:
            for bx in high:
                if bx.bar_seq < ax.bar_seq or bx.high < ax.low:
                    continue
                for cx in low:
                    if cx.bar_seq < bx.bar_seq or bx.high < cx.low:
                        continue
                    tall = bx.high - cx.low
                    if bc_tallest is None or bc_tallest < tall:
                        bc_tallest = tall
                    abc.append((ax, bx, cx))
        tall_abc = []
        for spec in abc:
            ax, bx, bc = spec
            # TODO filter here?
            if bx.high - cx.low < bc_tallest / 2:
                continue
            tall_abc.append(spec)
        return tall_abc

    def pivot_hlh(self):
        abc = []
        low, high = self.lows, self.highs
        bc_tallest = None
        for ax in high:
            for bx in low:
                if bx.bar_seq < ax.bar_seq or ax.high < bx.low:
                    continue
                for cx in high:
                    if cx.bar_seq < bx.bar_seq or cx.high < bx.low:
                        continue
                    tall = cx.high - bx.low
                    if bc_tallest is None or bc_tallest < tall:
                        bc_tallest = tall
                    abc.append((ax, bx, cx))
        tall_abc = []
        for spec in abc:
            ax, bx, bc = spec
            # TODO filter here?
            if cx.high - bx.low < bc_tallest / 2:
                continue
            tall_abc.append(spec)
        return tall_abc

    def gen_pitchforks(self):
        pf = []
        for abc in self.pivot_hlh():
            for type in PitchforkType.objects():
                pf.append(Pitchfork(self, PitchforkPattern.HIGH_LOW_HIGH,
                                    type, abc))
        for abc in self.pivot_lhl():
            for type in PitchforkType.objects():
                pf.append(Pitchfork(self, PitchforkPattern.LOW_HIGH_LOW,
                                    type, abc))
        return pf

    def gnuplot(self, out):
        out.write("""
set key off
set bars 1.2
set xtics rotate by 90 offset 0,-4.5
plot '-' using 1:2:3:4:5:xticlabels(6) with financebars linecolor rgb "black"
""")
        label_interval = int(self.len() / 12)
        i = 0
        for bar in self.bars:
            if i % label_interval == 0:
                when = '"' + bar.bar_time.strftime("%m-%d %H:%M") + '"'
            else:
                when = ''
            out.write("\t".join((str(bar.bar_seq), str(bar.open), str(bar.low),
                                 str(bar.high), str(bar.close), when)) + "\n")
            i += 1
        out.write("e\n")

    def show_major_pivots(self, out, color="red"):
        mark_pivots(self.lows, self.highs, out, color)


class Pitchfork:
    def __init__(self, orig, pattern, type, pivots):
        self.verbose = False
        self.orig = orig
        self.pattern = pattern
        self.type = type
        self.key_pivots = pivots
        self.initialize()

    def initialize(self):
        self.start = self.__get_start()
        self.cross = self.__get_cross()
        self.slope = self.__get_slope()
        self.height = self.__get_height()

    def get_raw_pivot(self, px):
        if bool(px & 1) ^ bool(self.pattern == PitchforkPattern.LOW_HIGH_LOW):
            y = self.key_pivots[px].low
            x = self.key_pivots[px].low_seq
        else:
            y = self.key_pivots[px].high
            x = self.key_pivots[px].high_seq
        return Point(x, y)

    def __get_start(self):
        if self.type == PitchforkType.ANDREWS:
            a = self.get_raw_pivot(0)
        elif self.type == PitchforkType.SCHIFF:
            a1 = self.get_raw_pivot(0)
            a2 = self.get_raw_pivot(1)
            a = Point(a1.x, (a1.y + a2.y) / 2)
        else:
            a1 = self.get_raw_pivot(0)
            a2 = self.get_raw_pivot(1)
            a = Point((a1.x + a2.x) / 2, (a1[1] + a2[1]) / 2)
        return a

    def __get_cross(self):
        b1 = self.get_raw_pivot(1)
        b2 = self.get_raw_pivot(2)
        b = Point((b1.x + b2.x) / 2, (b1.y + b2.y) / 2)
        return b

    def __get_slope(self):
        start = self.start
        cross = self.cross
        return (cross.y - start.y) / (cross.x - start.x)

    # Divide by two to place zero at the median line with
    # UP and LP at 1 and -1 respectively.
    def __get_height(self):
        b1 = self.get_raw_pivot(1)
        b2 = self.get_raw_pivot(2)
        dx = b2.x - b1.x
        b1 = Point(b1.x + dx, b1.y + dx * self.slope)
        h = abs(b1.y - b2.y) / 2
        if self.verbose:
            print(b1, b2, dx, h)
        return h

    def sp_start(self, h):
        b1, b2 = sorted([self.get_raw_pivot(px) for px in [1, 2]],
                        key=lambda p: p.y)
        cross = self.cross
        return Point(cross.x + (b2.x - b1.x) * h / 2,
                     cross.y + (b2.y - b1.y) * h / 2)

    # x axis is in bar_seq units, y axis in price
    def gnuplot(self, out, style_index=1, color="blue"):
        to = self.orig.last_bar
        # TODO don't really need to create a linestyle
        out.write("set style line {} ".format(style_index) +
                  'linecolor rgbcolor "{}"\n'.format(color))
        abc = [self.get_raw_pivot(px) for px in range(0, 3)]
        for p in abc:
            out.write("set object circle at first {},{}".format(*p) +
                      ' size 4 front fs empty border rgb "{}"\n'.format(color))
        # cross = self.cross
        # out.write("set object circle at first {},{}".format(*cross) +
        #           ' size 4 front fs empty border rgb "blue"\n')
        out.write("set arrow from first {},{} to first {},{} nohead ".
                  format(abc[1].x, abc[1].y, abc[2].x, abc[2].y) +
                  'linecolor rgbcolor "{}" linewidth 1\n'.format(color))
        slope = self.slope
        start = self.start
        out.write("set arrow from first {},{} to first {},{} nohead ".
                  format(start.x, start.y,
                         to, start.y + (to - start.x) * slope) +
                  "linestyle {} linewidth 2\n".format(style_index))
        for h in [-1, -.5, .5, 1] + [k for k in self.wl.keys()]:
            width = 1 if abs(h - round(h)) < .01 else .5
            tine = self.sp_start(h)
#            print(h, tine)
            out.write("set arrow from first {},{} to first {},{} nohead ".
                      format(tine.x, tine.y,
                             to, tine.y + (to - tine.x) * slope) +
                      "linestyle {} linewidth {}\n".format(style_index, width))

        vwidth = 11  # bar_seq units TODO
        for pricebar in self.validation:
            x, price = pricebar.coord
            out.write("set arrow from first {},{} to first {},{} nohead ".
                      format(x - vwidth, price - vwidth * slope,
                             x + vwidth, price + vwidth * slope) +
                      'linewidth 1 linecolor rgbcolor "{}"\n'.format("green"))

        for sp in self.sp:
            x1, y1 = sp[0].coord
            x2, ignore_y2 = sp[-1].coord
            out.write("set arrow from first {},{} to first {},{} nohead ".
                      format(x1, y1, x2, y1 + (x2 - x1) * slope) +
                      "linestyle {} linewidth .5\n".format(style_index))

        out.write("set style line {} default\n".format(style_index))

    def _from_orig_bar(self, bar):
        slope = self.slope
        height = self.height
        cross = self.cross
        adjust = lambda seq: cross.y + slope * (seq - cross.x)

        # Open & close use the same slope adjustment. This is wrong for
        # the close, but the close is not too important. TODO

        pb = Bar(bar_seq=bar.bar_seq,
                 high_seq=bar.high_seq,
                 low_seq=bar.low_seq,
                 bar_time=bar.bar_time,
                 open=(bar.open - adjust(bar.bar_seq)) / height,
                 high=(bar.high - adjust(bar.high_seq)) / height,
                 low=(bar.low - adjust(bar.low_seq)) / height,
                 close=(bar.close - adjust(bar.bar_seq)) / height)
        # if self.verbose and bar.bar_seq == 1024:
        #     print(slope, height, cross, bar.bar_seq, adjust(bar.bar_seq),
        #           bar.high, bar.low, '->', pb.high, pb.low)
        return pb

    def _translate_pivots(self):
        key_pivot_seq = [p.bar_seq for p in self.key_pivots]

        pivots = []
        for bar in self.orig.lows:
            if bar.bar_seq < self.start.x or bar.bar_seq in key_pivot_seq:
                continue
            pivots.append(Pivot(bar, self._from_orig_bar(bar), BarSide.LOW, 1))

        for bar in self.orig.highs:
            if bar.bar_seq < self.start.x or bar.bar_seq in key_pivot_seq:
                continue
            pivots.append(Pivot(bar, self._from_orig_bar(bar), BarSide.HIGH, 1))

        return pivots

    @property
    def window(self):
        bars = []
        for bar in w.bars[w.seq_to_index(self.start.x):w.len()]:
            bars.append(self._from_orig_bar(bar))
        return Window(bars)

    # prior to cross, only look for median touches (?)
    # after cross, consider any SP
    #
    # different kinds of validation:
    # - new SP
    # - touch on SP
    def calc_score(self):
        if self.verbose:
            print(self.slope)
        self.wl = collections.Counter()
        self.sp = []
        self.validation = []
        w = self.orig

        # TODO function of tick size, spread, and time scale?
        stretch = .05  # in fork height units

        majors = self._translate_pivots()

        # look for touchs on tines
        unloved = []
        for pricebar in majors:
            price = pricebar.price
            bar = pricebar.bar
            bin = price * 2
            if bar.bar_seq < self.cross.x and round(bin) != 0:
                unloved.append(pricebar)
                continue
            if abs(bin - round(bin)) < stretch:
                self.validation.append(pricebar.orig_bar)
                tine = round(bin) / 2
                if abs(tine) > 1:
                    # if self.verbose:
                    #     print(bin, abs(bin - round(bin)), tine)
                    #     print(self._to_orig_bar(pricebar))
                    #     self._from_orig_bar(self._to_orig_bar(pricebar)[1])
                    self.wl[tine] += 1
            else:
                unloved.append(pricebar)

        # search major pivots for sliding parallels
        # TODO there may be more than one solution and we pick arbitrarily
        majors = unloved
        majors = sorted(majors, key=lambda p: p.price)
        ax = 0
#        if self.verbose:
#            print([(p[1].bar_seq, p[0]) for p in majors])
        while ax < len(majors) - 1:
            p1 = majors[ax]
            best = None
            for bx in range(ax + 1, len(majors)):
                p2 = majors[bx]
#                if self.verbose:
#                    print('test', ax, bx, p2[0] - p1[0])
                if p2.price - p1.price < stretch:
                    best = bx
                else:
                    break
            if best is not None:
#                if self.verbose:
#                    print(self.slope,'run',ax,best)
                for pricebar in majors[ax:best + 1]:
                    self.validation.append(pricebar.orig_bar)
                orig = [pb.orig_bar for pb in majors[ax:best + 1]]
                self.sp.append(sorted(orig, key=lambda pb: pb.bar_seq))
                ax = best + 1
            else:
                ax = ax + 1

        self.check_constraints()

        self.score = len(self.validation)

        # pivots = std.find_all_pivots()
        # for bar in pivots.bars:
        #     x = bar.bar_seq
        #     if w.is_local_low(bar.bar_seq):
        #         pass
        #     if w.is_local_high(bar.bar_seq):
        #         pass

    def check_constraints(self):
        plist = [self.get_raw_pivot(px) for px in [1,2]]
        for rawp in plist:
            bar = self.orig.bars[self.orig.seq_to_index(rawp.x)]
            p = self._from_orig_bar(bar)
            if p.high > 0:
                if abs(p.high - 1) > .01:
                    print("WARN: ", rawp.x, p.high)
            if p.low < 0:
                if abs(p.low + 1) > .01:
                    print("WARN: ", rawp.x, p.low)
#            if self.verbose:
#            print(rawp.x, p.high, p.low)
        v = collections.Counter()
        for pricebar in self.validation:
            v[pricebar[1].bar_seq] += 1
        for bar, count in v.items():
            if count > 1:
                print(bar, "double counted")
