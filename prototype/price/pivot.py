import collections
from lib.enum import Enum

BarSide = Enum('BarSide', LOW=1, HIGH=2)
PivotData = collections.namedtuple('Pivot', 'private_orig_bar bar side weight')

class Pivot(PivotData):
    @property
    def orig_bar(self):
        if self.private_orig_bar is None:
            raise NameError("No original bar")
        return Pivot(None, self.private_orig_bar, self.side, self.weight)

    @property
    def price(self):
        if self.side == BarSide.LOW:
            return self.bar.low
        else:
            return self.bar.high

    @property
    def bar_seq(self):
        if self.side == BarSide.LOW:
            return self.bar.low_seq
        else:
            return self.bar.high_seq

    @property
    def coord(self):
        if self.side == BarSide.LOW:
            return self.bar.low_seq, self.bar.low
        else:
            return self.bar.high_seq, self.bar.high
