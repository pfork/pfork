import psycopg2
from psycopg2.extras import NamedTupleConnection

# TODO: factor DB creds
Db = psycopg2.connect(host="localhost", database="pfork",
                      user="pfork", password="pfork")


def get_last_id(tableid, columnid):
    cur = Db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
    try:
        cur.execute("select currval('%s_%s_seq')" % (tableid, columnid))
        return cur.fetchone()
    finally:
        cur.connection.commit()
        cur.close()
