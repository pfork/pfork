CREATE TABLE version (
    aspect text not null,
    version integer not null,
    primary key (aspect)
);
insert into version (aspect, version) values ('db', 1);
