CREATE TABLE vehicle (
vehicle_id serial PRIMARY KEY,
symbol text NOT NULL,
spread numeric
);

CREATE TABLE bar (
vehicle_id integer REFERENCES vehicle ON DELETE CASCADE,
duration smallint NOT NULL,
bar_seq integer,
bar_start timestamp with time zone NOT NULL,
open numeric NOT NULL,
close numeric NOT NULL,
high numeric NOT NULL,
low numeric NOT NULL,
high_time timestamp with time zone NOT NULL,
low_time timestamp with time zone NOT NULL,
PRIMARY KEY (vehicle_id, duration, bar_start)
);

CREATE TABLE analysis (
vehicle_id integer REFERENCES vehicle ON DELETE CASCADE,
start_time timestamp with time zone NOT NULL,
state text NOT NULL,
opened boolean NOT NULL DEFAULT('f'),
closed boolean NOT NULL DEFAULT('f'),
lots smallint,
gain_loss_pct double precision
);
