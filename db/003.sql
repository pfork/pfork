DELETE FROM bar;

ALTER TABLE bar
	DROP CONSTRAINT bar_pkey;

ALTER TABLE bar
	DROP COLUMN bar_start,
	DROP COLUMN high_time,
	DROP COLUMN low_time,
	ADD COLUMN bar_time timestamp with time zone NOT NULL,
	ADD COLUMN high_seq integer NOT NULL,
	ADD COLUMN low_seq integer NOT NULL,
	ALTER COLUMN bar_seq SET NOT NULL;

ALTER TABLE bar
	ADD CONSTRAINT bar_pkey PRIMARY KEY (vehicle_id, duration, bar_seq);
