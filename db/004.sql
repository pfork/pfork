
ALTER TABLE bar
	ALTER COLUMN "open" TYPE double precision /* TYPE change - table: bar original: numeric new: double precision */,
	ALTER COLUMN "close" TYPE double precision /* TYPE change - table: bar original: numeric new: double precision */,
	ALTER COLUMN high TYPE double precision /* TYPE change - table: bar original: numeric new: double precision */,
	ALTER COLUMN low TYPE double precision /* TYPE change - table: bar original: numeric new: double precision */;
